<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Backlog</div>

                <div class="panel-body">
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">To do</div>

                <div class="panel-body">
                </div>
            </div>
        </div>    

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">In Progress</div>

                <div class="panel-body">
                </div>
            </div>
        </div>    

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Done</div>

                <div class="panel-body">
                </div>
            </div>
        </div>                        
    </div>
</div>
