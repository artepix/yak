<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    App::setLocale('pt-BR');
    return view('welcome');
});

Auth::routes();

Route::get('/main', 'MainController@index');

Route::get('/main/layout', function() {
        return View::make('layouts.yak');
});

Route::get('/board', 'BoardController@index');