(function () {
    'use strict';

    angular.module('yak', ['ui.router'])

    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('app', {
                abstract: true
                , templateUrl: '/main/layout'
            })
            .state('app.board', {
                url: '/'
                , views: {
                    'page-content': {
                        templateUrl: '/board'
                    }
                }
            })            
        ;        
    })
    ;

    angular.element(document).ready(function () {
        angular.bootstrap(document, ['yak']);
    });    

})();